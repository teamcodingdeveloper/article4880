
## Requirement
- python3.8
- nodejs v.16


## Backend
Use FastAPI (https://fastapi.tiangolo.com/)

### Installation
```
cd backend && pip install -r requirement.txt
```
### Run
```
uvicorn app.main:app --reload
```


## Frontend
Use Nuxt 2 (https://v2.nuxt.com/docs/get-started/installation)


### Installation

```
cd ui && npm install
```

### Run
```
npm run dev
```

http://127.0.0.1:3000

## Test
Tested on ubuntu 22.04

![Atl imagekit](https://ik.imagekit.io/caripromo/Screenshot_from_2023-06-07_12-44-39_7cgCEwNMZ.png "image")
