-- Create table

create table posts (
    id int not null auto_increment,
    title varchar(200) not null,
    content text(5000) not null,
    category varchar(100) not null,
    created_date datetime default current_timestamp,
    updated_date datetime on update current_timestamp,
    status varchar(100) not null,
    PRIMARY KEY (id)
);

-- Create index for Post(PK)

create index posts_id_index 
on posts (id);

