from fastapi import FastAPI
from .routers import article
from fastapi.middleware.cors import CORSMiddleware

from .database import SessionLocal, engine
from . import models

# Auto mifrate models on database
models.Base.metadata.create_all(bind=engine)


origins = [
    'http://127.0.0.1:3000', 'http://127.0.0.1'
]


app = FastAPI(title='My Article API')
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(article.router, prefix='/article')


