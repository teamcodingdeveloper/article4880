from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel, validator
from sqlalchemy.orm import Session
import datetime as dt
from typing import Union, List
import math

from app.dependency import get_db
from app.models import Article



# Schema Inputan
class ArticleBase(BaseModel):
    title:str
    content:str
    category:str
    status:str = 'draft'

    @validator('title')
    def minimal_title_20(cls, v):
        if len(v) < 20:
            raise ValueError('minimal 20 char')
        
        return v
    

    @validator('content')
    def minimal_content_200(cls, v):
        if len(v) < 200:
            raise ValueError('minimal 200 char')
        
        return v
    
    @validator('category')
    def minimal_catevgory_3(cls, v):
        if len(v) < 3:
            raise ValueError('minimal 3 char')
        
        return v
    
    @validator('status')
    def available_status(cls, v):
        if v not in ['draft', 'publish', 'trash']:
            raise ValueError('Must be draft, publish, trash')
        
        return v


# Serializer for Response
class ArticleSchema(BaseModel):
    id:int
    title:str
    content:str
    category:str
    status:str

    class Config:
        orm_mode = True





router = APIRouter(tags=['Article'])



# List All
@router.get('/', response_model=List[ArticleSchema])
def all(status:Union[str, None]=None, db:Session=Depends(get_db)):
    data = db.query(
        Article
    )
    if status:
        data = data.filter(Article.status==status)

    data = data.order_by(Article.id.desc()).all()

    return data



# Create
@router.post('/')
def create(obj:ArticleBase, db:Session=Depends(get_db)):
    new = Article(title=obj.title, content=obj.content, category=obj.category, status=obj.status)
    db.add(new)
    db.commit()
    return {'created'}



# Detail
@router.get('/{id}', response_model=ArticleSchema)
def detail(id:int, db:Session=Depends(get_db)):
    data = db.query(
        Article
    ).filter(Article.id==id).first()

    if not data:
        raise HTTPException(404, detail='Article not found')

    return data



# Update
@router.put('/{id}')
def update(id:int, obj:ArticleBase, db:Session=Depends(get_db)):
    data = db.query(
        Article
    ).filter(Article.id==id).first()

    # Check data is exists
    if not data:
        raise HTTPException(404, detail='Article not found')

    data.title = obj.title
    data.content = obj.content
    data.category = obj.category
    data.status = obj.status
    db.commit()

    return {'Updated'}

# Limitaion List
@router.get('/{limit}/{offset}')
def paginate_list(publish:bool=False, limit:int=10, offset:int=0, db:Session=Depends(get_db)):
    data = db.query(
        Article
    )
    
    if publish:
        data = data.filter(
            Article.status=='publish'
        )

    count = data.count()

    data = data.order_by(Article.id.desc()) \
    .limit(limit).offset(offset).all()

    return {
        'results': data,
        'offset': offset,
        'limit': limit,
        'pages': math.ceil(count/limit)
    }


# Delete
@router.delete('/{id}')
def delete(id:int, db:Session=Depends(get_db)):
    data = db.query(
        Article
    ).filter(Article.id==id).first()

    # Check data existing
    if not data:
        raise HTTPException(404, detail='Data not exists')
    
    data.status = 'trash'
    db.commit()

    return {'Deleted'}


