import sqlalchemy as sa

from .database import Base


class Article(Base):
    '''
        Model for article table, named as 'posts' 
    '''
    __tablename__ = 'posts'

    id = sa.Column(sa.Integer, primary_key=True, index=True)
    title = sa.Column(sa.String(200))
    content = sa.Column(sa.Text)
    category = sa.Column(sa.String(100))
    created_date = sa.Column(sa.DateTime, server_default=sa.func.now())
    updated_date = sa.Column(sa.DateTime, onupdate=sa.func.now())
    status = sa.Column(sa.String(100))